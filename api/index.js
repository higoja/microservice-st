const bull = require('bull');

const redis = { host: '192.168.0.60', port: 6379 };

const opts = {redis: {host: redis.host, port: redis.port }};

const queueCreate = bull("curso:create", opts);

const queueDelete = bull("curso:delete", opts);

const queueUpdate = bull("curso:update", opts);

const queueFindOne = bull("curso:findOne", opts);

const queueView = bull("curso:View", opts);

async function Create({ age, name, color }) {

    try {
        const job = await  queueCreate.add({ age, name, color });

        const { statusCode, data, message } = await job.finished();

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
};

async function Delete({ id }) {

    try {
        const job = await queueDelete.add({ id });

        const { statusCode, data, message } = await job.finished();

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
};

async function Update({ age, name, color, id }) {

    try {
        const job = await queueUpdate.add({ age, name, color, id });

        const { statusCode, data, message } = await job.finished();

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
};

async function FindOne({ id }) {

    try {
        const job = await queueFindOne.add({ id });

        const { statusCode, data, message } = await job.finished();

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
};

async function View() {

    try {

        const job = await queueView.add({ });

        const { statusCode, data, message } = await job.finished();

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
};
module.exports = { Create, Delete, Update, FindOne, View };