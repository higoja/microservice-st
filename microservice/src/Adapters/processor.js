const Services = require('../Services');
const { InternalError } = require('../settings');

const { queueCreate, queueDelete, queueUpdate, queueFindOne, queueView } = require('./index');

async function View(job, done) {
    try {
        
        const { } = job.data;
  
        console.log(job.id);
        
        let { statusCode, data, message } = await Services.View({ });
        
        done(null, { statusCode, data, message });

    } catch (error) {
        console.log({step:'adapter queueView',error:error.toString()});

        return ;
        done(null, {statusCode:500, message: InternalError});
    }
};

async function Create(job, done) {
    try {
        
        const { age, name, color } = job.data;
        
        let { statusCode, data, message } = await Services.Create({ age, name, color });
        
        done(null, { statusCode, data, message });

    } catch (error) {
        console.log({step:'adapter queueCreate',error:error.toString()});

        return ;
        done(null, {statusCode:500, message: InternalError});
    }
};

async function Delete(job, done) {
    try {
        
        const { id } = job.data;
        
        let { statusCode, data, message } = await Services.Delete({ id });
        
        done(null, { statusCode, data, message });

    } catch (error) {
        console.log({step:'adapter queueDelete',error:error.toString()});

        return ;
        done(null, {statusCode:500, message: InternalError});
    }
};

async function Update(job, done) {
    try {
        
        const { age, name, color, id } = job.data;
        
        let { statusCode, data, message } = await Services.Update({ age, name, color,  id });
        
        done(null, { statusCode, data, message });

    } catch (error) {
        console.log({step:'adapter queueUpdate',error:error.toString()});

        return ;
        done(null, {statusCode:500, message: InternalError});
    }
};

async function FindOne(job, done) {
    try {
        
        const { id } = job.data;
        
        let { statusCode, data, message } = await Services.FindOne({ id });
        
        done(null, { statusCode, data, message });

    } catch (error) {
        console.log({step:'adapter queueFindOne',error:error.toString()});

        return ;
        done(null, {statusCode:500, message: InternalError});
    }
};

async function run() {
    try {

        console.log("Vamos a inicializar el worker...");

        queueView.process(View);
        
        queueCreate.process(Create);

        queueDelete.process(Delete);

        queueUpdate.process(Update);

        queueFindOne.process(FindOne);

    } catch (error) {
        console.log(error);
    }
};

module.exports = {
    Create, Delete, Update, FindOne, View, run
};