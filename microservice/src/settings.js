const dotenv = require('dotenv');
const { Sequelize } = require('sequelize');

// Se carga las dependencias y se ejecuta el metodo
// config() para tener disponible las variables de entorno
// declarados en el archivo .env
dotenv.config();

const redis = { host: process.env.REDIS_HOST, port: process.env.REDIS_PORT };


const InternalError = 'No podemos procesar tu solicitud en este momento...';

// ::: Se define y parametriza el objeto Sequelize con variables de entorno :::
// sequelize es el orm con el que se conecta a la
// base de datos para el CRUD
const sequelize = new Sequelize({
    host: process.env.POSTGRES_HOST, 
    port: process.env.POSTGRES_PORT,
    database: process.env.POSTGRES_DB, 
    username: process.env.POSTGRES_USER,
    password: process.env.POSTGRES_PASSWORD,
    dialect: 'postgres',
    logging: false
});

module.exports = { redis, InternalError, sequelize };