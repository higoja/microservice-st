// -- controladores : Obtiene, recibe o asigna datos
const { Model } = require('../Models');

async function Create({ name, age, color }) {
    try {
        let instance = await Model.create(
                { name, age, color }, 
                { fields: [ 'name', 'age', 'color' ] }
            );
            return { statusCode: 200, data: instance.toJSON() };
    } catch (error) {
        console.log({ step: 'controller Create', error: error.toString()});
        return { statusCode: 500, message: error.toString() };
    }
}

async function Delete({ where = {} }) {
    try {
        await Model.destroy({ where });

        return { statusCode: 200, data: "OK" };
    } catch (error) {
        console.log({ step: 'controller Delete', error: error.toString()});
        return { statusCode: 500, message: error.toString() };
    }
}

async function Update({ name, age, color, id }) {
    try {
        let instance = await Model.update( 
            { name , age, color, id },
            { where: { id }, returning: true }
            );

        return { statusCode: 200, data: instance[1][0].toJSON()};

    } catch (error) {
        console.log({ step: 'controller Update', error: error.toString()});
        return { statusCode: 500, message: error.toString() };
    }
}

async function FindOne({ where = {} }) {
    try {
        let instance = await Model.findOne({ where });
        
        if (instance) return { statusCode: 200, data: instance.toJSON()};
        
        else return { statusCode: 400, message: "No existe el usuario" };

    } catch (error) {
        console.log({ step: 'controller FindOne', error: error.toString()});
        return { statusCode: 500, message: error.toString() };
    }
}

async function View({ where = {}}) {
    try {
        let instances = await Model.findAll({ where });
        return { statusCode: 200, data: instances };
    } catch (error) {
        console.log({ step: 'controller View', error: error.toString()});
        return { statusCode: 500, message: error.toString() };
    }
}

module.exports = { Create, Delete, Update, FindOne, View };