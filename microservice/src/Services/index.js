// -- servicios : procesa lo obtenido en los controladores
const Controllers = require('../Controllers') ;
const { InternalError } = require('../settings');

async function Create({ age, name, color }) {

    try {

        let { statusCode, data, message } = await Controllers.Create({age, name, color});
        
        return { statusCode, data, message };

    } catch (error) {
        
        console.log({step:'service Create',error:error.toString()});
        
        return {statusCode:500, message:error.toString()};
    }
}

async function Delete({ id }) {

    try {

        const findOne = await Controllers.FindOne({ where: { id } });

        if (findOne.statusCode !== 200) {
            
            /*let response = {
                400: { statusCode:400, message: "No existe el usuario a eliminar"},
                500: { statusCode:500, message: InternalError}
            }

            return response[findOne.statusCode];
            */

            switch (findOne.statusCode) {
                case 400: return { statusCode: 400, message: "No existe el usuario a eliminar"}

                case 500: return { statusCode: 500, message: InternalError}

                default: return { statusCode: findOne.statusCode, message: findOne.message}
            }



        }

        const del = await Controllers.Delete({where: { id }});
        
        if (del.statusCode === 200) return { statusCode: 200, data: findOne.data };

        return { statusCode: 500, message: error.toString };

    } catch (error) {
        
        console.log({step:'service Delete',error:error.toString()});
        
        return {statusCode:500, message:error.toString()};
    }
}

async function Update({ age, name, color, id }) {

    try {

        let { statusCode, data, message } = await Controllers.Update({age, name, color, id });
        
        return { statusCode, data, message };

    } catch (error) {
        
        console.log({step:'service Update',error:error.toString()});
        
        return {statusCode:500, message:error.toString()};
    }
}

async function FindOne({ id }) {

    try {

        let { statusCode, data, message } = await Controllers.FindOne({ where: { id } });
        
        return { statusCode, data, message };

    } catch (error) {
        
        console.log({step:'service FindOne',error:error.toString()});
        
        return {statusCode:500, message:error.toString()};
    }
}

async function View({}) {

    try {

        let { statusCode, data, message } = await Controllers.View({});
        
        return { statusCode, data, message };

    } catch (error) {
        
        console.log({step:'service View',error:error.toString()});
        
        return {statusCode:500, message:error.toString()};
    }
}

module.exports = { Create, Delete, Update, FindOne, View };