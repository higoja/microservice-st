const { sequelize } = require('../settings')
const { DataTypes } = require('sequelize');

// se crea modelo con sequelize
const Model = sequelize.define( 'curso' ,{
    name: {type: DataTypes.STRING},
    age: {type: DataTypes.BIGINT},
    color: {type: DataTypes.STRING}
});

//sequelize.define();

// Define funcion para sincronizar modelo con db
async function SyncDB() {
    try {
        // se sincriniza modelo con db

        console.log('se sincroniza modelo con db...');
        await Model.sync();

        return { statusCode: 200, data: "Ok" };

    } catch (error) {

        console.log(error);

        return { statusCode: 500, message: error.toString() };
    }
}


module.exports = { Model, SyncDB };
