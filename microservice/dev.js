const micro = require('./src');

async function main() {
    try {
        
        console.log('se aguarda conexion a db...');

        const db = await micro.SyncDB();

        console.log('db conectada y sincronizada con modelo...');

        if (db.statusCode !== 200) throw db.message

        console.log('::: se inicializa el microservicio :::');
        
        micro.run();

        console.log('::: microservicio listo y escuchando ::: (: = :)');

    } catch (error) {
        
        console.log(error);
        
    }
}

main();