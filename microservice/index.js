const{ Adaptador } = require('./src/Adapters');

const main = async () => {
    try {
        console.log('LLAMADA AL ADAPTADOR');
        const result  = await Adaptador({ id: 1 });

        if (result.statusCode !== 200) throw(result.message);
        
        console.log( "Tu Data es:", result.data );
        
    } catch (error) {
        console.log(error);
    }
};

main();