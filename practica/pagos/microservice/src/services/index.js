const Controllers = require('../controllers');

async function Create({ socio, amount }) {

    try {

        let { statusCode, data, message } = await Controllers.Create({ socio, amount });

        return { statusCode, data, message };

    } catch (error) {

        console.log({ step: 'services Create', error: error.toString() });

        return { statusCode: 500, data: '', message: error.toString() }
    }
};

async function Delete({ id }) {

    try {

        const findOne = await Controllers.FindOne({ where: { id } });

        if (findOne.statusCode !== 200) {

            switch (findOne.statusCode) {
                case 400: return { statusCode: 400, message: "The payment to delete does not exist" }

                case 500: return { statusCode: 500, message: InternalError }

                default: return { statusCode: findOne.statusCode, message: findOne.message }
            }
        }

        const del = await Controllers.Delete({ where: { id } });

        if (del.statusCode === 200) return { statusCode: 200, data: findOne.data }

        else return { statusCode: 500, message: "Delete:" + del.message };

    } catch (error) {

        console.log({ step: 'service Delete', error: error.toString() });

        return { statusCode: 500, message: error.toString() };
    }
};

async function FindOne({ id }) {

    try {

        let { statusCode, data, message } = await Controllers.FindOne({ where: { id } });

        return { statusCode, data, message };

    } catch (error) {

        console.log({ step: 'service FindOne', error: error.toString() });

        return { statusCode: 500, message: error.toString() };
    }
};

async function View({ }) {

    try {

        let { statusCode, data, message } = await Controllers.View({ });

        return { statusCode, data: data, message };

    } catch (error) {

        console.log({ step: 'services View', error: error.toString() });

        return { statusCode: 500, data: '', message: error.toString() }
    }
};

module.exports = { Create, Delete, FindOne, View };