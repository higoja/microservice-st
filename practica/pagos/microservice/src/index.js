const { SyncDB } = require('./models/index');
const { run } = require('./adapters/index');

module.exports = { SyncDB, run };