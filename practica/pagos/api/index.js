const bull = require('bull');

const nameApi = require('./package.json');

const name = nameApi.name.replace('api-', '');

//console.log(nameApi.name);
//console.log(name);

/*
const { redis, name } = require('../microservice/src/setting');
No es viable ya que al empaquetar no se encotrara siempre con la misma ruta
al utilizarlo en otros micro
*/

const redis = { host: 'localhost', port: 6379 };

const opts = { redis: { host: redis.host, port: redis.port } };

const queueCreate = bull(`${name}:create`, opts);
const queueDelete = bull(`${name}:delete`, opts);
const queueFindOne = bull(`${name}:findOne`, opts);
const queueView = bull(`${name}:view`, opts);

async function Create({ socio, amount }) {

    try {
        const job = await queueCreate.add({ socio, amount });

        const { statusCode, data, message } = await job.finished();

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
};

async function Delete({ id }) {

    try {
        const job = await queueDelete.add({ id });

        const { statusCode, data, message } = await job.finished();

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
};

async function FindOne({ id }) {

    try {
        const job = await queueFindOne.add({ id });

        const { statusCode, data, message } = await job.finished();

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
};

async function View({ }) {

    try {
        const job = await queueView.add({});

        const { statusCode, data, message } = await job.finished();

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
};

module.exports = { Create, Delete, FindOne, View };