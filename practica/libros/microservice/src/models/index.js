const { sequelize } = require('../setting');
const { DataTypes } = require('sequelize');
const { name } = require('../setting');


const Model = sequelize.define(name, {
    title: { type: DataTypes.STRING },
    category: { type: DataTypes.STRING },
    seccions: { type: DataTypes.STRING }
});

async function SyncDB() {

    try {

        await Model.sync();

        return { statusCode: 200, data: 'ok' };

    } catch (error) {

        console.log("There was an error in the synchronization", error);

        return { statusCode: 500, message: error.toString() };

    }
};
module.exports = { Model, SyncDB }