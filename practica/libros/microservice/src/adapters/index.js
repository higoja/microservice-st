const Services = require('../services');

const { queueCreate, queueDelete, queueUpdate, queueFindOne, queueView } = require('./setting');

const { InternalError } = require('../setting');

async function Create(job, done) {

    try {

        const { title } = job.data;

        let { statusCode, data, message } = await Services.Create({ title });

        done(null, { statusCode, data, message });

    } catch (error) {

        console.log({ step: 'adapter queueCreate', error: error.toString() });

        return;
        done(null, { statusCode: 500, data: '', message: InternalError });
    }
};

async function Delete(job, done) {

    try {

        const { id } = job.data;

        let { statusCode, data, message } = await Services.Delete({ id });

        done(null, { statusCode, data, message });

    } catch (error) {

        console.log({ step: 'adapter queueDelete', error: error.toString() });

        return;
        done(null, { statusCode: 500, data: '', message: InternalError });
    }
};

async function Update(job, done) {

    try {

        const { id, title, category, seccions } = job.data;

        let { statusCode, data, message } = await Services.Update({ id, title, category, seccions });

        done(null, { statusCode, data, message });

    } catch (error) {

        console.log({ step: 'adapter queueUpdate', error: error.toString() });

        return;
        done(null, { statusCode: 500, data: '', message: InternalError });
    }
};

async function FindOne(job, done) {
    try {

        const { id } = job.data;

        let { statusCode, data, message } = await Services.FindOne({ id });

        done(null, { statusCode, data, message });

    } catch (error) {
        console.log({ step: 'adapter queueFindOne', error: error.toString() });

        return;
        done(null, { statusCode: 500, message: InternalError });
    }
};

async function View(job, done) {

    try {

        const { } = job.data;

        let { statusCode, data, message } = await Services.View({});

        done(null, { statusCode, data, message });

    } catch (error) {

        console.log({ step: 'adapter queueView', error: error.toString() });

        return;
        done(null, { statusCode: 500, data: '', message: InternalError });
    }
};

async function run() {
    try {

        console.log("Inicializa el worker...:");

        queueCreate.process(Create);
        queueDelete.process(Delete);
        queueUpdate.process(Update);
        queueFindOne.process(FindOne);
        queueView.process(View);

    } catch (error) {
        console.log(error);
    }
};

module.exports = { Create, Delete, Update, FindOne, View, run };