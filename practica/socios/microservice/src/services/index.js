const Controllers = require('../controllers');

async function Create({ name, phone }) {

    try {

        let { statusCode, data, message } = await Controllers.Create({ name, phone });

        return { statusCode, data, message };

    } catch (error) {

        console.log({ step: 'services Create', error: error.toString() });

        return { statusCode: 500, data: '', message: error.toString() }
    }
};

async function Delete({ id }) {

    try {

        const findOne = await Controllers.FindOne({ where: { id } });

        if (findOne.statusCode !== 200) {

            switch (findOne.statusCode) {
                case 400: return { statusCode: 400, message: "The partner to delete does not exist" }

                case 500: return { statusCode: 500, message: InternalError }

                default: return { statusCode: findOne.statusCode, message: findOne.message }
            }
        }

        const del = await Controllers.Delete({ where: { id } });

        if (del.statusCode === 200) return { statusCode: 200, data: findOne.data }

        else return { statusCode: 500, message: "Delete:" + del.message };

    } catch (error) {

        console.log({ step: 'service Delete', error: error.toString() });

        return { statusCode: 500, message: error.toString() };
    }
};

async function Update({ id, name, age, email, phone }) {

    try {

        const findOneUpd = await Controllers.FindOne({ where: { id } });

        if (findOneUpd.statusCode !== 200) {

            switch (findOneUpd.statusCode) {
                case 400: return { statusCode: 400, message: "The partner to update does not exist" }

                case 500: return { statusCode: 500, message: InternalError }

                default: return { statusCode: findOneUpd.statusCode, message: findOneUpd.message }
            }
        }

        const upd = await Controllers.Update({ id, name, age, email, phone });

        if (upd.statusCode === 200) return { statusCode: upd.statusCode, data: upd.data }

        else return { statusCode: upd.statusCode, message: "Update:" + upd.message };

    } catch (error) {

        console.log({ step: 'services Update', error: error.toString() });

        return { statusCode: 500, data: '', message: error.toString() }
    }
};

async function FindOne({ id }) {

    try {

        let { statusCode, data, message } = await Controllers.FindOne({ where: { id } });

        return { statusCode, data, message };

    } catch (error) {

        console.log({ step: 'service FindOne', error: error.toString() });

        return { statusCode: 500, message: error.toString() };
    }
};

async function View({ enable }) {

    try {

        let { statusCode, data, message } = await Controllers.View({ where: { enable } });

        return { statusCode, data: data, message };

    } catch (error) {

        console.log({ step: 'services View', error: error.toString() });

        return { statusCode: 500, data: '', message: error.toString() }
    }
};

async function Enable({ id }) {

    try {

        const findOneEna = await Controllers.FindOne({ where: { id } });

        if (findOneEna.statusCode !== 200) {

            switch (findOneEna.statusCode) {
                case 400: return { statusCode: 400, message: "The partner to enable does not exist" }

                case 500: return { statusCode: 500, message: InternalError }

                default: return { statusCode: findOneEna.statusCode, message: findOneEna.message }
            }
        }

        const ena = await Controllers.Enable({ id });

        if (ena.statusCode === 200) return { statusCode: ena.statusCode, data: ena.data }

        else return { statusCode: ena.statusCode, message: "Enable:" + ena.message };

    } catch (error) {

        console.log({ step: 'services Enable', error: error.toString() });

        return { statusCode: 500, data: '', message: error.toString() }
    }
};

async function Disable({ id }) {

    try {

        const findOneDis = await Controllers.FindOne({ where: { id } });

        if (findOneDis.statusCode !== 200) {

            switch (findOneDis.statusCode) {
                case 400: return { statusCode: 400, message: "The partner to disable does not exist" }

                case 500: return { statusCode: 500, message: InternalError }

                default: return { statusCode: findOneDis.statusCode, message: findOneDis.message }
            }
        }

        const dis = await Controllers.Disable({ id });

        if (dis.statusCode === 200) return { statusCode: dis.statusCode, data: dis.data }

        else return { statusCode: dis.statusCode, message: "Disable:" + dis.message };

    } catch (error) {

        console.log({ step: 'services Disable', error: error.toString() });

        return { statusCode: 500, data: '', message: error.toString() }
    }
};

module.exports = { Create, Delete, Update, FindOne, View, Enable, Disable };