const { sequelize } = require('../setting');
const { DataTypes } = require('sequelize');
const { name } = require('../setting');


const Model = sequelize.define(name, {
    name: { type: DataTypes.STRING },
    age: { type: DataTypes.BIGINT },
    email: { type: DataTypes.STRING },
    phone: { type: DataTypes.STRING },
    enable: { type: DataTypes.BOOLEAN, defaultValue: true }
});

async function SyncDB() {

    try {

        await Model.sync();

        return { statusCode: 200, data: 'ok' };

    } catch (error) {

        console.log("There was an error in the synchronization", error);

        return { statusCode: 500, message: error.toString() };

    }
};
module.exports = { Model, SyncDB }