const express = require('express');
const http = require('http');

const api = require('../api');

const app = express();
const server = http.createServer(app);

const { Server } = require('socket.io');

const io = new Server(server);

server.listen(80, () => {
    console.log("Server inicialized...");

    io.on('connection', socket => {
        console.log("New connection", socket.id );

        socket.on('req:microservice:view', async ({ }) => {

            try {

                console.log('req:microservice:view');

                const { statusCode, data, message } = await api.View({ });

                io.to(socket.id).emit('res:microservice:view', { statusCode, data, message });
                
            } catch (error) {

                console.log(error);
                
            }
        });

        socket.on('req:microservice:create', async ({ age, name, color }) => {

            try {

                console.log('req:microservice:create', ({ age, name, color }));
                
                const { statusCode, data, message } = await api.Create({ age, name, color });

                io.to(socket.id).emit('res:microservice:create', { statusCode, data, message });
                
            } catch (error) {

                console.log(error);
                
            }

        })

        socket.on('req:microservice:delete', async ({ id }) => {

            try {

                console.log('req:microservice:delete', ({ id }));

                const { statusCode, data, message } = await api.Delete({ id });

                io.to(socket.id).emit('res:microservice:delete', { statusCode, data, message });
                
            } catch (error) {

                console.log(error);
                
            }
        });

        socket.on('req:microservice:update', async ({ age, name, color, id }) => {
            try {
                console.log('req:microservice:update', ({ age, name, color, id }));

                const { statusCode, data, message } = await api.Update({ age, name,color, id });

                io.to(socket.id).emit('res:microservice:update', { statusCode, data, message });
                
            } catch (error) {

                console.log(error);
                
            }
        });

        socket.on('req:microservice:findone', async ({ id }) => {

            try {

                console.log('req:microservice:findone', ({ id }));

                const { statusCode, data, message } = await api.FindOne({ id });

                io.to(socket.id).emit('res:microservice:findone', { statusCode, data, message });
                
            } catch (error) {

                console.log(error);
                
            }
        });
    })
});
